import {
  Box,
  Button,
  Card,
  createTheme,
  Grid,
  Typography,
} from "@mui/material";
import { connect } from "react-redux";
import {
  addTodos,
  completeTodos,
  removeTodos,
  updateTodos,
} from "../redux/reducer";
import React, { Fragment } from "react";
import clsx from "clsx";
import { makeStyles } from "@mui/styles";
import { todo } from "./homePage";
import CustomListItem from "../common/CustomListItem";
import { Circle } from "@mui/icons-material";
import ViewTodo from "./viewTodo";
// const useStyles = makeStyles({

//     actionContainer: {
//       position: "relative",
//       height: "100%",
//     },

//     cancelButton: {
//       bottom: theme.spacing(1),
//       right: theme.spacing(1),
//       position: "absolute",
//       maxHeight: "30px",
//     },
//     acceptButton: {
//       color: "green",
//       border: "1px solid #4CAF50",
//       right: "97px",
//       bottom: theme.spacing(1),
//       position: "absolute",
//       maxHeight: "30px",
//     },
// });
const theme = createTheme({
  spacing: [0, 4, 8, 16, 32, 64],
});
const useStyles: any = makeStyles({
  root: {
    background: "grey",
    cursor: "pointer",
    "&:hover": {
      background: "#fafafa",
      boxShadow: "1px 1px 10px rgba(0, 0, 0, 0.2)",
      cursor: "pointer",
    },
  },
  borderLeft: {
    borderLeft: "1px solid #e0e0e0",
  },
  actionContainer: {
    position: "relative",
    height: "100%",
  },
  statusChip: {
    top: theme.spacing(1),
    right: theme.spacing(1),
    position: "absolute",
    maxHeight: "20px",
  },
});
const mapStateToProps = (state: any) => {
  return {
    todos: state,
  };
};

const mapDispatchToProps = (
  dispatch: (arg0: { payload: any; type: string }) => any
) => {
  return {
    removeTodo: (id: any) => dispatch(removeTodos(id)),
    updateTodo: (obj: any) => dispatch(updateTodos(obj)),
    completeTodo: (id: any) => dispatch(completeTodos(id)),
  };
};
interface TodoListProps {
  todo: todo;
}
function TodoList(props: TodoListProps) {
  const [openView, setOpenView] = React.useState(false);
  const color =
    props.todo.priorty == "low"
      ? "green"
      : props.todo.priorty == "medium"
      ? "yellow"
      : "red";
  const classes = useStyles();
  return (
    <Fragment>
      <Card
        className={clsx("card-shadow", classes.root)}
        onClick={() => setOpenView(true)}
      >
        <Grid container className={clsx("pt-2 pl-2", classes.borderLeft)}>
          <Grid item md={9}>
            <CustomListItem
              reverse={true}
              label={props.todo.description}
              value={props.todo.title}
            />
          </Grid>
          <Grid
            item
            md={3}
            sx={{ alignItems: "rigth" }}
            className={classes.borderLeft}
          >
            <Box className={classes.actionContainer}>
              <>
                <Grid className={classes.statusChip}>
                  <Typography sx={{ alignItems: "center" }}>
                    {props.todo.priorty.toUpperCase()}
                    &nbsp;
                    <Circle sx={{ color: color }}></Circle>
                  </Typography>
                </Grid>
                <Button
                  onClick={() => {}}
                  sx={{ backgroundColor: "orange", color: "black" }}
                >
                  <Typography> Edit Task</Typography>
                </Button>
                &nbsp;
                <Button
                  onClick={() => {
                    completeTodos(props.todo.id);
                  }}
                  sx={{ backgroundColor: "green", color: "black" }}
                >
                  <Typography> Done Task</Typography>
                </Button>
              </>
            </Box>
          </Grid>
        </Grid>
      </Card>
      {openView && (
        <ViewTodo
          open={openView}
          setOpenView={setOpenView}
          todo={props.todo}
        ></ViewTodo>
      )}
    </Fragment>
  );
}
export default connect(mapStateToProps, mapDispatchToProps)(TodoList);
