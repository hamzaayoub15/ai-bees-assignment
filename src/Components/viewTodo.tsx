import React, { Fragment } from "react";
import { todo } from "./homePage";
import Button from "@mui/material/Button";
import { styled } from "@mui/material/styles";
import Dialog from "@mui/material/Dialog";
import DialogTitle from "@mui/material/DialogTitle";
import DialogContent from "@mui/material/DialogContent";
import DialogActions from "@mui/material/DialogActions";
import IconButton from "@mui/material/IconButton";
import CloseIcon from "@mui/icons-material/Close";
import Typography from "@mui/material/Typography";
import { Circle } from "@mui/icons-material";
import { Grid } from "@mui/material";
import {
  addTodos,
  completeTodos,
  removeTodos,
  updateTodos,
} from "../redux/reducer";
import { connect } from "react-redux";
import store from "../redux/store";
const mapStateToProps = (state: any) => {
  return {
    todos: state,
  };
};

const mapDispatchToProps = (
  dispatch: (arg0: { payload: any; type: string }) => any
) => {
  return {
    removeTodo: (id: any) => dispatch(removeTodos(id)),
    updateTodo: (obj: any) => dispatch(updateTodos(obj)),
    completeTodo: (id: any) => dispatch(completeTodos(id)),
  };
};
const BootstrapDialog = styled(Dialog)(({ theme }) => ({
  "& .MuiDialogContent-root": {
    padding: theme.spacing(2),
    width: "4500px",
  },
  "& .MuiDialogActions-root": {
    padding: theme.spacing(1),
    // justifyContent: "center",
  },
}));

export interface DialogTitleProps {
  id: string;
  children?: React.ReactNode;
  onClose: () => void;
}

const BootstrapDialogTitle = (props: DialogTitleProps) => {
  const { children, onClose, ...other } = props;

  return (
    <DialogTitle sx={{ m: 0, p: 2 }} {...other}>
      {children}
      {onClose ? (
        <IconButton
          aria-label="close"
          onClick={onClose}
          sx={{
            position: "absolute",
            right: 8,
            top: 8,
            color: (theme) => theme.palette.grey[500],
          }}
        >
          <CloseIcon />
        </IconButton>
      ) : null}
    </DialogTitle>
  );
};
interface ViewTodoProps {
  todo: todo;
  open: boolean;
  setOpenView: (arg: boolean) => void;
  removeTodo?: (arg0: { id: number }) => void;
}
function ViewTodo(props: ViewTodoProps) {
  console.log("props", props);

  const handleClose = () => {
    props.setOpenView(false);
  };
  const color =
    props.todo.priorty == "low"
      ? "green"
      : props.todo.priorty == "medium"
      ? "yellow"
      : "red";

  return (
    <div>
      <BootstrapDialog
        onClose={handleClose}
        aria-labelledby="customized-dialog-title"
        open={props.open}
        sx={{ cursor: "pointer" }}
      >
        <BootstrapDialogTitle
          id="customized-dialog-title"
          onClose={handleClose}
        >
          {props.todo.priorty.toUpperCase()}
          &nbsp;
          <Circle sx={{ color: color }}></Circle>
          <Typography
            sx={{ position: "relative", top: "-28px", right: "-244px" }}
          >
            {" "}
            <strong>{props.todo.title}</strong>
          </Typography>
        </BootstrapDialogTitle>
        <DialogContent dividers>
          <Typography gutterBottom>{props.todo.description}</Typography>
        </DialogContent>
        <DialogActions>
          <Grid sx={{ paddingRight: "100px" }}>
            <Button
              onClick={() => {
                handleClose();
              }}
              sx={{ backgroundColor: "orange", color: "black" }}
            >
              <Typography> Edit Task</Typography>
            </Button>
          </Grid>
          <Grid sx={{ paddingRight: "100px" }}>
            <Button
              onClick={() => {
                handleClose();
              }}
              sx={{ backgroundColor: "green", color: "black" }}
            >
              <Typography> Done Task</Typography>
            </Button>
          </Grid>
          <Grid>
            <Button
              onClick={() => {
                props.removeTodo?.({ id: props.todo.id });
                handleClose();
              }}
              sx={{ backgroundColor: "red", color: "black" }}
            >
              <Typography> Delete Task</Typography>
            </Button>
          </Grid>
        </DialogActions>
      </BootstrapDialog>
    </div>
  );
}
export default connect(mapStateToProps, mapDispatchToProps)(ViewTodo);
