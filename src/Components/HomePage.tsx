import {
  Button,
  Card,
  CardActions,
  CardContent,
  Grid,
  Typography,
} from "@mui/material";
import React, { Fragment, useEffect, useState } from "react";
import { makeStyles } from "@mui/styles";
import AddTodo from "./addTodo";
import store from "../redux/store";
import TodoList from "./todoList";
import { connect } from "react-redux";
const useStyles = makeStyles({
  root: {
    minWidth: "275",
    border: "1px solid black",
    height: "80vh",
    width: "100%",
    borderRadius: ".5em",
    padding: "10px",
  },
  button: {
    border: "1px solid black",
    height: "40px",
    width: "235px",
    margin: "-20px -50px",
    position: "relative",
    top: "35%",
    left: "45%",
    backgroundColor: "orange",
    alignItems: "center",
  },
});
const mapStateToProps = (state: any) => {
  return {
    todos: state,
  };
};
export interface todos {
  id?: number;
  title?: string;
  description?: string;
  priorty?: string;
  compeleted?: boolean;
}
export interface todo {
  id: number;
  title: string;
  description: string;
  priorty: string;
  compeleted: boolean;
}
function HomePage(props: any) {
  console.log(props);
  const classes = useStyles();
  const [openAddTask, setAddTask] = useState<boolean>(false);

  return (
    <Fragment>
      <Grid>
        <Grid className={classes.root}>
          <Fragment>
            <CardContent>
              <Typography
                sx={{ fontSize: 14 }}
                color="text.secondary"
                gutterBottom
              >
                <h1 style={{ textAlign: "center" }}>Todo App</h1>
              </Typography>
            </CardContent>
            {props.todos.length < 1 && (
              <Grid className={classes.button}>
                <Button onClick={() => setAddTask(true)}>
                  <Typography>Create your first task</Typography>
                </Button>
              </Grid>
            )}
            {openAddTask && props.todos.length < 1 && (
              <AddTodo open={openAddTask} setOpen={setAddTask} />
            )}
            {props.todos.length > 0 && (
              <Grid container spacing={2}>
                {props.todos?.map((todo: todo) => (
                  <Grid key={todo?.id} item xs={12}>
                    <TodoList todo={todo} />
                  </Grid>
                ))}
              </Grid>
            )}
          </Fragment>
        </Grid>
      </Grid>
    </Fragment>
  );
}
export default connect(mapStateToProps)(HomePage);
