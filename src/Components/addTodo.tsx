import * as React from "react";
import Button from "@mui/material/Button";
import { styled } from "@mui/material/styles";
import Dialog from "@mui/material/Dialog";
import DialogTitle from "@mui/material/DialogTitle";
import DialogContent from "@mui/material/DialogContent";
import DialogActions from "@mui/material/DialogActions";
import IconButton from "@mui/material/IconButton";
import CloseIcon from "@mui/icons-material/Close";
import Typography from "@mui/material/Typography";
import { connect } from "react-redux";
import { addTodos } from "../redux/reducer";
import {
  FormControl,
  FormControlLabel,
  Grid,
  Radio,
  RadioGroup,
  TextField,
} from "@mui/material";
import { green, red, yellow } from "@mui/material/colors";
const BootstrapDialog = styled(Dialog)(({ theme }) => ({
  "& .MuiDialogContent-root": {
    padding: theme.spacing(2),
    width: "4500px",
  },
  "& .MuiDialogActions-root": {
    padding: theme.spacing(1),
    justifyContent: "center",
  },
}));

export interface DialogTitleProps {
  id: string;
  children?: React.ReactNode;
  onClose: () => void;
}
const BootstrapDialogTitle = (props: DialogTitleProps) => {
  const { children, onClose, ...other } = props;

  return (
    <DialogTitle sx={{ m: 0, p: 2 }} {...other}>
      {children}
      {onClose ? (
        <IconButton
          aria-label="close"
          onClick={onClose}
          sx={{
            position: "absolute",
            right: 8,
            top: 8,
            color: (theme) => theme.palette.grey[400],
          }}
        >
          <CloseIcon />
        </IconButton>
      ) : null}
    </DialogTitle>
  );
};
const mapStateToProps = (state: any) => {
  return {
    todos: state,
  };
};

const mapDispatchToProps = (
  dispatch: (arg0: { payload: any; type: string }) => any
) => {
  return {
    addTodo: (obj: any) => dispatch(addTodos(obj)),
  };
};
export interface AddDialogProps {
  addTodo?: (arg0: {
    id: number;
    title: string;
    description: string;
    priorty: string;
    completed: boolean;
  }) => void;
  open: boolean;
  setOpen: (arg0: boolean) => void;
}

function AddTodo(props: AddDialogProps) {
  const [title, setTitle] = React.useState("");
  const [description, setDescription] = React.useState("");
  const [priorty, setPriority] = React.useState("");
  const add = () => {
    props.addTodo?.({
      id: Math.floor(Math.random() * 1000),
      title: title,
      description: description,
      priorty: priorty,
      completed: false,
    });
    setTitle("");
    setDescription("");
    setPriority("");
  };

  const { open, setOpen } = props;
  const handleClose = () => {
    setOpen(false);
  };
  return (
    <React.Fragment>
      <BootstrapDialog
        onClose={handleClose}
        aria-labelledby="customized-dialog-title"
        open={open}
      >
        <BootstrapDialogTitle
          id="customized-dialog-title"
          onClose={handleClose}
        >
          Add Task
        </BootstrapDialogTitle>
        <DialogContent dividers>
          <FormControl>
            <TextField
              required
              id="outlined-required"
              sx={{ width: "450px" }}
              label="Title"
              onChange={(event) => {
                setTitle(event.target.value);
              }}
              value={title}
            />
            <br></br>
            <TextField
              required
              id="required"
              sx={{ width: "450px" }}
              label="Description"
              onChange={(event) => {
                setDescription(event.target.value);
              }}
              value={description}
            />
            <RadioGroup
              row
              aria-labelledby="demo-row-radio-buttons-group-label"
              name="row-radio-buttons-group"
              onChange={(event) => {
                setPriority(event.target.value);
              }}
            >
              <FormControlLabel
                value="low"
                control={
                  <Radio
                    sx={{
                      "&.Mui-checked": {
                        color: green[600],
                      },
                    }}
                  />
                }
                label="Low"
              />
              <FormControlLabel
                style={{ marginLeft: "100px" }}
                value="medium"
                control={
                  <Radio
                    sx={{
                      "&.Mui-checked": {
                        color: yellow[600],
                      },
                    }}
                  />
                }
                label="Medium"
              />
              <FormControlLabel
                sx={{
                  marginLeft: "100px",
                }}
                value="high"
                control={
                  <Radio
                    sx={{
                      "&.Mui-checked": {
                        color: red[600],
                      },
                    }}
                  />
                }
                label="High"
              />
            </RadioGroup>
          </FormControl>
        </DialogContent>
        <DialogActions>
          <Grid>
            <Button
              disabled={
                !(
                  title.length > 0 &&
                  description.length > 0 &&
                  priorty.length > 0
                )
              }
              onClick={() => {
                add();
                handleClose();
              }}
              sx={{ backgroundColor: "orange" }}
            >
              <Typography> Add To Task</Typography>
            </Button>
          </Grid>
        </DialogActions>
      </BootstrapDialog>
    </React.Fragment>
  );
}
export default connect(mapStateToProps, mapDispatchToProps)(AddTodo);
