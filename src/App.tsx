import React, { Fragment } from "react";
import "./App.css";
import HomePage from "./components/homePage";

function App() {
  return (
    <Fragment>
      <HomePage />
    </Fragment>
  );
}

export default App;
