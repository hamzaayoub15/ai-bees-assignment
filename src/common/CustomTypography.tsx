import { Typography } from "@mui/material";
import { makeStyles } from "@mui/styles";
import clsx from "clsx";

const textComponentStyles = makeStyles({
  rightAligned: {
    textAlign: "right",
  },
  primaryText: {
    fontSize: "13px",
    fontWeight: 400,
    wordBreak: "break-word",
  },
  secondaryText: {
    fontSize: "12px",
    color: "#787878",
    wordBreak: "break-word",
  },
  linkStyle: {
    color: "#4088cc",
  },
});

interface TextProps {
  text?: string | JSX.Element | false | undefined | number;
  rightAligned?: boolean;
  url?: string;
  className?: string;
}

interface TextCoreProps extends TextProps {
  type: "primary" | "secondary";
}

export function TextComponent(props: TextCoreProps) {
  const { text, rightAligned, className } = props;
  const classes = textComponentStyles();
  return (
    <Typography
      variant={props.type === "primary" ? "body1" : "body2"}
      className={clsx({
        [classes.rightAligned]: rightAligned,
        [classes.primaryText]: props.type === "primary",
        [classes.secondaryText]: props.type === "secondary",
      })}
    >
      {text}
    </Typography>
  );
}

export function PrimaryText(props: TextProps) {
  return TextComponent({ ...props, type: "primary" });
}

export function SecondaryText(props: TextProps) {
  return TextComponent({ ...props, type: "secondary" });
}
