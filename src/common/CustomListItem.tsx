import { Box, createTheme } from "@mui/material";
import { makeStyles } from "@mui/styles";
import { Fragment } from "react";
import { SecondaryText, PrimaryText } from "./CustomTypography";
import clsx from "clsx";

interface CustomListItemProps {
  label?: string | number | JSX.Element | undefined;
  value?: string | number | JSX.Element | undefined;
  icon?: JSX.Element;
  url?: string;
  reverse?: boolean;
  onlyItem?: boolean;
  bold?: boolean;
  textCenter?: boolean;
  huge?: boolean;
  tiny?: boolean;
}
const theme = createTheme({
  spacing: [0, 4, 8, 16, 32, 64],
});

const useStyles: any = makeStyles({
  root: {
    display: "flex",
    flexDirection: "row",
    alignItems: "center",
    paddingBottom: "12px",
  },
  onlyItem: {
    paddingBottom: "0px!important",
  },
  reverse: {
    display: "flex",
    flexDirection: "column-reverse",
  },
  iconContainer: {
    display: "flex",
    alignItems: "center",
    marginBottom: "2px",
    color: "#cfcfcf",
  },
  listContainer: {
    paddingLeft: theme.spacing(1),
  },
  boldText: {
    "& .MuiTypography-body1": {
      fontWeight: "600!important",
      letterSpacing: 0.1,
    },
  },
  textCenter: {
    justifyContent: "center",
    "& .MuiTypography-body1, & .MuiTypography-body2": {
      textAlign: "center",
    },
  },
  tiny: {
    "& .MuiTypography-body2": {
      fontSize: "9px!important",
    },
    "& .MuiTypography-body1": {
      fontSize: "11px!important",
    },
  },
  huge: {
    transform: "scale(1.5)",
  },
});

function CustomListItem(props: CustomListItemProps) {
  const classes = useStyles();
  const {
    label,
    value,
    icon,
    reverse,
    url,
    onlyItem,
    bold,
    textCenter,
    huge,
    tiny,
  } = props;

  return (
    <Fragment>
      <Box
        className={clsx(classes.root, {
          [classes.onlyItem]: onlyItem,
          [classes.textCenter]: textCenter,
          [classes.huge]: huge,
          [classes.tiny]: tiny,
        })}
      >
        {icon && <Box className={classes.iconContainer}>{icon}</Box>}
        <Box
          className={clsx({
            [classes.reverse]: reverse,
            [classes.listContainer]: !!icon,
            [classes.boldText]: bold,
          })}
        >
          <SecondaryText text={label} />
          <PrimaryText text={value} url={url} />
        </Box>
      </Box>
    </Fragment>
  );
}

export default CustomListItem;
