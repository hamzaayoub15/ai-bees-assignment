import { createSlice } from "@reduxjs/toolkit";
import { todo } from "../components/homePage";

const initialState: any = [];
interface IAction {
  type: string;
  payload: any;
}
const addTodoReducer = createSlice({
  name: "todos",
  initialState,
  reducers: {
    //here we will write our reducer
    //Adding todos
    addTodos: (state, action: IAction) => {
      state.push(action.payload);
      return state;
    },
    //remove todos
    removeTodos: (state, action: IAction) => {
      console.log("state", action.payload);
      return state.filter(
        (todo: { id: number }) => todo.id !== action.payload.id
      );
    },
    //update todos
    updateTodos: (state, action: IAction) => {
      return state.map((todo: { id: any }) => {
        if (todo.id === action.payload.id) {
          return {
            ...todo,
            item: action.payload.item,
          };
        }
        return todo;
      });
    },
    //completed
    completeTodos: (state, action) => {
      return state.map((todo: { id: any }) => {
        if (todo.id === action.payload) {
          return {
            ...todo,
            completed: true,
          };
        }
        return todo;
      });
    },
  },
});

export const { addTodos, removeTodos, updateTodos, completeTodos } =
  addTodoReducer.actions;
export const reducer = addTodoReducer.reducer;
